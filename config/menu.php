<?php

return [
    'footer' => [
        'col-1' => [
            [
                'url' => '/',
                'title' => 'الرئيسية',
                'type' => 'url',
            ],
            [
                'url' => 'من نحن',
                'type' => 'page',
                'page_id' => 7,
            ],
            [
                'url' => 'login',
                'type' => 'page',
                'page_id' => 3,
            ],
            [
                'url' => 'login',
                'type' => 'page',
                'page_id' => 4,
            ],
        ],
        'col-2' => [
            [
                'title' => 'معاهدة الاستخدام',
                'type' => 'page',
                'page_id' => 6,
            ],
            [
                'title' => 'شروط الاستخدام',
                'type' => 'page',
                'page_id' => 8,
            ],
            [
                'title' => 'الخصم',
                'type' => 'page',
                'page_id' => 9,
            ],
        ],
        'col-3' => [
            [
                'title' => 'عضوية الشركات والمؤسسات',
                'type' => 'page',
                'page_id' => 13,
            ],
            [
                'title' => 'الحسابات البنكية',
                'type' => 'page',
                'page_id' => 16,
            ],
            [
                'title' => 'العضوية المميزة',
                'type' => 'page',
                'page_id' => 11,
            ],
//            [
//                'title' => 'نظام التقييم',
//                'type' => 'page',
//                'page_id' => 12,
//            ],
        ],
        'col-4' => [
            [
                'url' => '/commision',
                'title' => 'حساب العمولة',
                'type' => 'url',
            ],
            [
                'url' => '/advertise',
                'title' => 'الإعلانات',
                'type' => 'url',
            ],
            [
                'url' => '/blacklist',
                'title' => 'القائمة السوداء',
                'type' => 'url',
            ],
        ],
        'col-5' => [
            [
                'url' => '/contactus',
                'title' => 'الإقتراحات والشكاوي',
                'type' => 'url',
            ],
            [
                'url' => '/jobs',
                'title' => 'التوظيف',
                'type' => 'url',
            ],
            [
                'url' => '/contactus',
                'title' => 'اتصل بنا',
                'type' => 'url',
            ],
        ],
    ],
    'mobile' => [
//        [
//            'url' => '/',
//            'title' => 'الرئيسية',
//            'type' => 'url',
//            'icon' => 'fas fa-home'
//        ],
//        [
//            'title' => 'الملف الشخصي',
//            'in_login' => true,
//            'url' => 'users/admin/edit',
//            'type' => 'url',
//            'icon' => 'fas fa-user'
//        ],
        [
            'url' => '/favourites',
            'title' => 'الإعلانات المفضلة',
            'in_login' => true,
            'type' => 'url',
            'icon' => 'far fa-heart'
        ],
        [
            'url' => '/notfications',
            'title' => 'الإشعارات',
            'in_login' => true,
            'type' => 'url',
            'icon' => 'fas fa-bell'
        ],
        [
            'url' => '/chat',
            'title' => 'الرسائل',
            'in_login' => true,
            'type' => 'url',
            'icon' => 'fas fa-comments'
        ],
        [
            'url' => '/categories',
            'title' => 'الأقسام',
            'type' => 'url',
            'icon' => 'fas fa-th'
        ],
        [
            'url' => '/commision',
            'title' => 'حساب العمولة',
            'type' => 'url',
            'icon' => 'fas fa-calculator'
        ],
        [
            'url' => '/',
            'title' => 'دفع العمولة',
            'in_login' => true,
            'type' => 'url',
            'icon' => 'fas fa-credit-card'
        ],
        [
            'url' => '/',
            'title' => 'الحسابات البنكية',
            'in_login' => true,
            'type' => 'url',
            'icon' => 'fas fa-university'
        ],
        [
//            'url' => '/',
//            'title' => 'الشروط والأحكام',
//            'in_login' => true,
//            'type' => 'url',
            
            
            'title' => 'شروط الاستخدام',
//          'in_login' => TRUE,
            'type' => 'page',
            'page_id' => 8,
            'icon' => 'fas fa-gavel',
        ],
        [
            'url' => '/contactus',
            'title' => 'اتصل بنا',
            'type' => 'url',
            'icon' => 'fas fa-envelope'
        ],
        [
            'url' => '/',
            'title' => 'الإعلانات المدفوعة',
//            'in_login' => true,
            'type' => 'page',
            'page_id' => 15,
            'icon' => 'fas fa-star'
        ],
        [
            'url' => '/logout',
            'title' => 'تسجيل الخروج',
            'in_login' => true,
            'type' => 'url',
            'icon' => 'fas fa-sign-out-alt'
        ],
    ],
    'top' => [
        [
            'url' => '/',
            'title' => 'الرئيسية',
            'type' => 'url',
        ],
//        [
//            'url' => 'login',
//            'title' => 'الدخول',
//            'in_login' => FALSE,
//            'type' => 'url',
//        ],
        [
            'url' => 'login',
            'title' => 'من نحن',
            'type' => 'page',
            'page_id' => 7,
        ],
        [
            'url' => 'advertise',
            'title' => 'الإعلانات',
            'type' => 'url',
        ],
        [
            'url' => 'login',
            'title' => 'معاهدة استخدام الموقع',
            'type' => 'page',
            'page_id' => 6,
        ],
        [
            'url' => 'commision',
            'title' => 'حساب العمولة',
            'type' => 'url',
        ],
        [
            'url' => 'contactus',
            'title' => 'اتصل بنا',
            'type' => 'url',
        ],
    ]
];
