<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    protected $table='images';
    protected $guarded = ['id'];
	protected $hidden = ['created_at','updated_at'];

	public function advs(){
		return $this->belongsTo('App\Advs');
	}
	public function getImageAttribute($img){
		return $img ? url('/').$img : '';
	}
}
