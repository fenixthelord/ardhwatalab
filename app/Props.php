<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Props extends Model
{
	protected $table = 'proprites';
	protected $guarded = ['id'];
	protected $hidden = ['created_at', 'updated_at'];

	public function dept()
	{
		return $this->belongsTo('App\Depts');
	}
	public function parent()
	{
		return $this->belongsTo('App\Props');
	}
	public function types()
	{
		return $this->hasMany('\App\PropTypes', 'prop_id');
	}

	public function title()
	{
		return $this->belongsTo(Titles::class, 'title_id');
	}
}
