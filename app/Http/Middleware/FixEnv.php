<?php

namespace App\Http\Middleware;

use Closure;

class FixEnv {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if(isset($_COOKIE['debug'])) {
                return $next($request);
        } else {
            if (!\App\SiteConfig::first()->close) {
                return $next($request);
            }
            return response()->view('fix_env');
        }
    }

}
