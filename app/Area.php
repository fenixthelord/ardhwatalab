<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table='area';
    protected $guarded = ['id'];
	protected $hidden = ['created_at','updated_at'];
	
    public function country(){
    	return $this->belongsTo('App\Area');
    }
    public function advs(){
    	return $this->hasMany('App\Advs','area');
    }
}
