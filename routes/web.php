<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::auth();

Route::group(['middleware' => 'guest', 'namespace' => 'Auth'], function () {
    // Authentication Routes...
    Route::match(['post', 'get'], 'login', ['as' => 'login', 'uses' => 'AuthController@login']);
    Route::match(['post', 'get'], 'signup', ['as' => 'signup', 'uses' => 'AuthController@signup']);
    Route::get('oauth/login/{driver}', ['as' => 'slogin', 'uses' => 'SocialAuth@getSocialAuth']);
    Route::get('oauth/callback', ['as' => 'callback', 'uses' => 'SocialAuth@callback']);
    Route::post('password_reset', 'AuthController@password_reset')->name('password_reset');
    Route::get('password/reset/{token}', 'AuthController@reset_password')->name('reset_password');
});
Route::match(['get', 'post'], 'users/active', ['as' => 'users.active', 'uses' => 'Auth\AuthController@user_active']);
Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@logout', 'middleware' => 'auth']);
Route::get('active/{id}/{token}', ['as' => 'auth.active', 'uses' => 'Auth\AuthController@active']);
Route::get('lawyer_logout', function () {
    auth('lawyer')->logout();
    return redirect()->to('/');
});





Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return 'Application cache cleared';
});


// b9550d85211bcd9f1861171f9c7edf5c.png
Route::get('/imageresize/medium/{photo}', function ($photo) {
    $photo_hash = md5($photo);
    $imagee = new Intervention\Image\ImageManagerStatic;
    if (!Cache::has($photo_hash)) {
        $image_encode = (string) $imagee->make(public_path($photo))
            ->resize(200, 200)
            ->encode('png', 100);
        Cache::forever($photo_hash, $image_encode);
    }
    header("Content-type: image/png");
    header('Expires: Wed, 21 Oct 2099 07:28:00 GMT');
    echo (string) Cache::get($photo_hash);
    die;
})->name('SPhone')->where('photo', '.*');


// Route::get('/testme', function() {
//     $_token = \DB::table('password_resets')->where('email', 'nebras400@gmail.com')->first();
//     if(is_null($_token)) {
//         return 'null';
//     } else {
//         dd($_token);
//     }
// });

Route::group(['namespace' => 'Site' , 'middleware' => 'fix_env'], function () {



    Route::match(['get', 'post'], 'jobs', ['as' => 'jobs', 'uses' => 'PageController@jobs']);
    Route::get('/', ['as' => 'home', 'uses' => 'IndexController@home']);
    Route::get('/home', ['uses' => 'IndexController@home']);
    Route::get('last', ['as' => 'advs.last', 'uses' => 'AdvsController@last']);
    Route::get('requests', ['as' => 'advs.requests', 'uses' => 'AdvsController@requests']);
    Route::get('country/{country}', ['as' => 'country', 'uses' => 'AdvsController@country']);
    Route::get('area/{area}', ['as' => 'area', 'uses' => 'AdvsController@area']);
    Route::get('category/{cat}/{cat_name}', ['as' => 'cats', 'uses' => 'AdvsController@cat']);
    Route::get('categories', ['as' => 'categories', 'uses' => 'AdvsController@cats']);
    Route::get('types/{name}', ['as' => 'types', 'uses' => 'AdvsController@types']);
    Route::get('search', ['as' => 'search', 'uses' => 'SearchController@search']);
    Route::get('searchdetails', ['as' => 'searchdetails', 'uses' => 'SearchController@getDetails']);
    Route::match(['get', 'post'], 'contactus', ['as' => 'contactus', 'uses' => 'PageController@contactus']);
    Route::resource('users', 'Profile');
    Route::group(['middleware' => 'auth'], function () {
        Route::any('select-subscription/{adv_id}', '\App\Http\Controllers\SubscriptionController@index')->name('select.subscription');
        Route::get('users/{username}/messages', ['as' => 'users.msgs', 'uses' => 'Profile@msgs', 'middleware' => 'auth']);
        Route::post('updatecontacts', ['as' => 'contacts', 'uses' => 'Profile@contacts']);
        Route::group(['middleware' => 'active'], function () {
            Route::get('user/shop/edit', ['as' => 'users.shop.edit', 'uses' => 'Profile@update']);
            Route::post('deladvs', ['as' => 'deladvs', 'uses' => 'AdvsController@remove']);
            Route::match(['get', 'post'], 'chat/{userid?}', ['as' => 'users.chat', 'uses' => 'ActionsController@chat']);
            Route::get('user/requests/{type}', ['as' => 'users.requests', 'uses' => 'Profile@requests']);
            Route::get('user/ads/{type?}', ['as' => 'users.advs', 'uses' => 'Profile@advs']);
            Route::get('user/followers/{id?}', ['as' => 'users.follow', 'uses' => 'Profile@follow']);
            Route::post('user/rates/{id}', ['as' => 'users.rates', 'uses' => 'Profile@rate2']);
            Route::get('user/rate/{id}/{type}', ['as' => 'users.rate', 'uses' => 'Profile@rate', 'middleware' => 'systems:rating']);
            Route::get('timeline', ['as' => 'users.timeline', 'uses' => 'Profile@timeline']);
            Route::get('claims/{type}', ['as' => 'users.claims', 'uses' => 'Profile@claims']);
            Route::get('user/joins', ['as' => 'users.joins', 'uses' => 'Profile@joins', 'middleware' => 'systems:joins']);
        });
        Route::get('documentation_form', ['as' => 'users.documentation_form', 'uses' => 'Profile@documentation_form', 'middleware' => 'auth']);
        Route::post('documentation_form', ['as' => 'users.documentation_form', 'uses' => 'Profile@documentation_form_post', 'middleware' => 'auth']);
    });
    Route::resource('advertise', 'AdvsController');
    Route::get('advertise/{id}/remove', 'AdvsController@remove')->name('advertise.remove');
    Route::get('advertise/{id}/republished', 'AdvsController@republished')->name('advertise.republished');
    Route::get('advertise/{id}/{slug}', ['as' => 'advertise.show', 'uses' => 'AdvsController@show']);
    Route::get('advertise-print/{id}', ['as' => 'advertise.print', 'uses' => 'AdvsController@print']);

    // commision
    Route::get('commision', ['as' => 'commision', 'uses' => 'PageController@commision']);
    Route::post('commision', ['as' => 'commision.store', 'uses' => 'PageController@commision_post']);
    Route::get('commision_respons', ['as' => 'commision.back', 'uses' => 'PageController@payment_callback']);
    // commision

    // Dexter
    //Route::get('commision_Dex', ['as' => 'commision_Dex', 'uses' => 'PageController@commision_Dex']);
    Route::post('commision_Dex', 'PageController@commision_post_Dex')->name('store_Dex');
    Route::get('commision_respons_DEx','PageController@payment_callback_DEx')->name('payment_callback_DEx');
    // Dexter

    Route::get('blacklist', ['as' => 'blacklist', 'uses' => 'IndexController@blacklist']);
    Route::group(['middleware' => 'active'], function () {
        Route::get('remove-image/{id}', ['as' => 'remove-image', 'uses' => 'AdvsController@remove_image']);
        Route::get('advertise/delete/{slug}/{id}', ['as' => 'advertise.destroy', 'uses' => 'AdvsController@destroy']);
        Route::match(['get', 'post'], 'report/{title}/{comment?}', ['as' => 'advertise.claims', 'uses' => 'AdvsController@claim', 'middleware' => 'auth']);
        // Route::match(['get','post'],'like/{id}/{type}',['as'=>'advertise.like','uses'=>'AdvsController@like','middleware'=>'auth']);
        Route::match(['get', 'post'], 'mediation', ['as' => 'advertise.median', 'uses' => 'AdvsController@median', 'middleware' => ['auth', 'systems:media']]);
        Route::match(['get', 'post'], 'actions/{id}/{slug}', ['as' => 'advertise.actions', 'uses' => 'AdvsController@actions', 'middleware' => 'auth']);
        Route::match(['get', 'post'], 'transfer', ['as' => 'transfer', 'uses' => 'PageController@transfer']);
        Route::get('favourites', ['as' => 'likes', 'uses' => 'ActionsController@likes', 'middleware' => 'auth']);
        Route::any('rates/{id}', ['as' => 'rates', 'uses' => 'ActionsController@rates']);
        Route::get('like/{id}/{user?}/{like?}', ['as' => 'like', 'uses' => 'ActionsController@like', 'middleware' => 'auth']);
        Route::post('bids', ['as' => 'bids', 'uses' => 'ActionsController@bids', 'middleware' => 'auth']);
        Route::post('comment', ['as' => 'comment', 'uses' => 'ActionsController@comment', 'middleware' => 'auth']);
        Route::get('list-bids', ['as' => 'list-bids', 'uses' => 'Profile@list_bids']);
        Route::get('notfications', ['as' => 'user.notfs', 'uses' => 'Profile@notfs']);
    });
    Route::get('page/{id}/{title}', ['as' => 'page', 'uses' => 'IndexController@page']);
    Route::get('joins', ['as' => 'joins', 'uses' => 'PageController@joins', 'middleware' => 'systems:joins']);
    Route::match(['get', 'post'], 'joins/request/{type}', ['as' => 'joins.request', 'uses' => 'PageController@getjoins', 'middleware' => 'active', 'middleware' => 'systems:joins']);
    Route::get('banking', ['as' => 'banking', 'uses' => 'PageController@banking']);
    Route::get('error/{type?}', ['as' => 'errors', 'uses' => 'PageController@error']);
    /* fm */
    Route::get('get-areas/{country_id}', ['as' => 'get-areas', 'uses' => 'ActionsController@getAreas']);
    Route::get('by-location/{country_id}/{area_id}', ['as' => 'get-areas', 'uses' => 'AdvsController@byLocation']);
});
Route::get('getDetails', [
    'as' => 'getDetails',
    'uses' => '\App\Http\Controllers\Admin\AdvsController@getDetails'
]);
use Carbon\Carbon ;
use App\Chat;
use App\Notfs;
use App\User;
Route::get("obaida" , function(){
    $advs = App\Advs::where('active' , 1)->where('end_date' , "<=" , Carbon::now())->get()->each(function($data){
        dd($data);
        $data->update([
            'active' => 0]);
    });
    return $advs;

});

Route::get('dayleftcron' , function (){
    $advs = App\Advs::where('active' , 1)->where('end_date' , "<=" , Carbon::now()->addDays(1))->get();
    // dd($advs);
    if (count($advs) < 1){
        return "no items" ;

    }
    foreach($advs as $adv){
        //   dd(Carbon::now()->diffInDays($adv->end_date));
        if(Carbon::now()->diffInDays($adv->end_date) == 1){
            Notfs::create([

                'user_id'        => $adv->user_id,
                'link' =>  route('select.subscription', ['adv_id' => $adv->id]),

                'text'   => "
            	        عمينا العزيز نفيدكم بأن مدة الاعلان رقم $adv->id ستنتهي خلال يوم من الوقت المحدد أدناه وتفادياً لإيقاف الاعلان يمكنكم تجديد الاشتراك بالضغط هنا
            	        "
            ]);
            echo "1 day later for advs id $adv->id <br>" ;
        }
        else{
            echo 'no item dey left';
        }
    }
});
Route::get("testcrone" , function(){
    $advs = App\Advs::where('active' , 1)->where('end_date' , "<=" , Carbon::now()->addDays(2))->get();
    foreach($advs as $adv){
        //   dd(Carbon::now()->diffInDays($adv->end_date));

        if(Carbon::now()->gte($adv->end_date)){
            Notfs::create([

                'user_id'        => $adv->user_id,
                'link' =>  route('select.subscription', ['adv_id' => $adv->id]),
                'text'   => "
            	        عميلنا العزيز نفيدكم علماً بأنه تم ايقاف الاعلان الخاص بكم ورقمه $adv->id للتجديد اضغط هنا
            	        "
            ]);
            $adv->update([
                'active' => 0 ,
            ]);
            echo "adv {$adv->id} is inactive <br>" ;
        }
        else{
            echo "no item active <br>" ;
        }
    }

});

Route::get('/members', 'Site\MembersController@index')->middleware('auth')->name('select-members');
// Route::get('DSFDSFSDFSDF', function(){
//     $array = '';
//     $i = 1;
//     foreach(\App\Depts::where('parent_id' , null)->get() as $dept) {
//         $array .= drawMenuDeskTop($dept);
//         $i++;
//     }
//     dd($array);
// });



Route::group(['middleware' => ['auth', 'admin'], 'namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::get('/', ['as' => 'admin', 'uses' => 'IndexController@home']);
    Route::post('remove', ['as' => 'admin.remove', 'uses' => 'IndexController@remove']);
    Route::group(['prefix' => 'config'], function () {
        Route::match(['get', 'post'], '/', ['as' => 'admin.site_config', 'uses' => 'IndexController@site_config']);

        //Route::match(['get', 'post'], 'lawsuit_txt', ['as' => 'admin.lawsuit_txt', 'uses' => 'IndexController@lawsuit_txt']);
        Route::match(['get', 'post'], 'contacts', ['as' => 'admin.contacts', 'uses' => 'IndexController@contacts']);
        Route::match(['get', 'post'], 'contactus', ['as' => 'admin.contactus', 'uses' => 'IndexController@contactus']);
        Route::match(['get', 'post'], 'aboutus', ['as' => 'admin.aboutus', 'uses' => 'IndexController@aboutus']);
        Route::match(['get', 'post'], 'apps', ['as' => 'admin.apps', 'uses' => 'IndexController@apps']);
        Route::match(['get', 'post'], 'systems', ['as' => 'admin.systems', 'uses' => 'IndexController@systems']);
        Route::match(['get', 'post'], 'mail', ['as' => 'admin.mail', 'uses' => 'IndexController@mail']);
        Route::match(['get', 'post'], 'sms', ['as' => 'admin.sms', 'uses' => 'IndexController@sms']);
        Route::match(['get', 'post'], 'close', ['as' => 'admin.close', 'uses' => 'IndexController@close']);
    });
    Route::match(['get', 'post'], 'commissions', ['as' => 'admin.commissions', 'uses' => 'CommissionsController@index']);
    Route::match(['get', 'post'], 'advs_config/{view}', ['as' => 'admin.advs.config', 'uses' => 'IndexController@advs_config']);


    Route::get('advs/{adv}/rePublic', 'AdvsController@rePublic')->name('admin.advs.rePublic');

    Route::get('advs/{adv}/pdf', 'AdvsController@pdf')->name('admin.advs.pdf')->where('adv','[0-9]+');


    // Dexter
    // Message
    Route::get ('/Message', 'IndexController@site_Message')->name('message');
    Route::post('/Message', 'IndexController@site_Message');
    // Members
    // add new members
    Route::get ('members/new', 'MembersController@CreateNew')->name('CreateMember');
    Route::post('members/new', 'MembersController@CreateNew');

    // View members
    Route::get ('members/manage', 'MembersController@ManageMember')->name('ManageMember');
    // Edit
    Route::get ('members/edit/{id}', 'MembersController@EditMember')->name('EditMember');
    Route::post('members/edit/{id}', 'MembersController@EditMember');
    // Delete
    Route::get ('members/remove/{id}', 'MembersController@RemoveMember')->name('RemoveMember');

    // Dexter


    Route::resource('advs', 'AdvsController');
    //    Route::get('getDetails', ['as' => 'getDetails', 'uses' => 'AdvsController@getDetails']);
    Route::resource('depts', 'DeptsController');
    /*Route::resource('law_categories', 'LawCategoriesController');
    Route::resource('lawyers', 'LawyersController');
    Route::get('lawyer/requests', 'LawyersController@requests')->name('admin.lawyers.requests');
    Route::resource('lawsuits', 'LawsuitsController');
    Route::match(['get', 'post'], 'lawsuit/{id}/lawyers', 'LawsuitsController@lawyers')->name('admin.lawsuits.lawyers');
    Route::get('choose_lawyer/{id}', 'LawsuitsController@choose_lawyer')->name('choose_lawyer');
    Route::get('un_choose_lawyer/{id}', 'LawsuitsController@un_choose_lawyer')->name('un_choose_lawyer');
    Route::get('approve_lawyer/{id}', 'LawsuitsController@approve_lawyer')->name('approve_lawyer');*/
    Route::resource('props', 'PropsController');
    Route::resource('titles', 'TitlesController');
    Route::get('getProps', ['as' => 'admin.getprops', 'uses' => 'PropsController@getProps']);
    Route::get('getTitles', ['as' => 'admin.gettitles', 'uses' => 'PropsController@getTitles']);
    Route::post('delPropTypes', ['as' => 'admin.proptypes.delete', 'uses' => 'PropsController@delPropTypes']);
    Route::resource('pages', 'PagesController');
    Route::resource('operations', 'OperationsController');
    Route::resource('peroids', 'PeroidsController');
    Route::resource('country', 'CountryController');
    Route::resource('area', 'AreaController');
    Route::resource('paymethods', 'PaymethodsController');
    Route::resource('currency', 'CurrencyController');
    Route::resource('jointypes', 'JointypesController');
    Route::resource('posters', 'PostersController');
    Route::resource('sliders', 'SlidersController');
    Route::resource('users', 'UserController');
    Route::get('user/joins/requests', ['as' => 'admin.users.requests', 'uses' => 'UserController@requests']);
    Route::resource('roles', 'RolesController');
    Route::get('pays', ['as' => 'admin.pays', 'uses' => 'IndexController@pays']);
    Route::get('jobs', ['as' => 'admin.jobs', 'uses' => 'IndexController@jobs']);
    Route::get('contactus', ['as' => 'admin.contactus', 'uses' => 'IndexController@contactus']);
    Route::get('bankTransfer', 'BankTransferController@index')->name('bankTransfer');
    Route::get('claim', 'ClaimController@index')->name('admin.claim');
    //reports
    Route::get('/reports/users', 'ReportesController@users')->name('admin.reports.users');
    Route::get('/reports/advs', 'ReportesController@advs')->name('admin.reports.advs');
    Route::get('/reports/orders', 'ReportesController@orders')->name('admin.reports.orders');


    // Send Notifcation
    Route::get('/notifcation', 'NotifcationController@index')->name('admin.notifcation.index');
    Route::post('/notifcation', 'NotifcationController@store')->name('admin.notifcation.store');


    Route::get('/getDetailss', function() {
        if(request()->has('country')) {
            $lists = \App\Area::where(['country_id'=>request('country')])->get();
            return view('ajax.area')->with(['area'=>$lists])->render();
        }
        if(request()->has('area')) {
            // dd(request('area'));
            $lists = \App\Lawyer::where(['area_id'=>request('area'),'status'=>1])->get();
            return view('ajax.lawyerss')->with(['lists'=>$lists])->render();
        }
        return '';
    });

    // AreaLawyers



    // Documentation Form Catgeories
    Route::get('/documentation/category', 'DocumentationCatgeoryController@index')->name('admin.documentation.category.index');
    Route::get('/documentation/category/create', 'DocumentationCatgeoryController@create')->name('admin.documentation.category.create');
    Route::post('/documentation/category/create', 'DocumentationCatgeoryController@store')->name('admin.documentation.category.store');
    Route::get('/documentation/category/{category}/edit', 'DocumentationCatgeoryController@edit')->name('admin.documentation.category.edit')->where('category','[0-9]+');;
    Route::put('/documentation/category/{category}/update', 'DocumentationCatgeoryController@update')->name('admin.documentation.category.update')->where('category','[0-9]+');;

    // Documentation Form
    Route::get('/documentation', 'DocumentationController@index')->name('admin.documentation.index');
    Route::get('/documentation/{documentation}/show', 'DocumentationController@show')->name('admin.documentation.show')->where('documentation','[0-9]+');
    Route::get('/documentation/{documentation}/edit', 'DocumentationController@edit')->name('admin.documentation.edit')->where('documentation','[0-9]+');
    Route::put('/documentation/{documentation}/update', 'DocumentationController@update')->name('admin.documentation.update')->where('documentation','[0-9]+');
    Route::get('/documentation/{documentation}/activeted', 'DocumentationController@activeted')->name('admin.documentation.activeted')->where('documentation','[0-9]+');


    // subscriptions Form Catgeories
    Route::get('/subscriptions', 'SubscriptionController@index')->name('admin.subscription.index');
    Route::get('/subscriptions/create', 'SubscriptionController@create')->name('admin.subscription.create');
    Route::post('/subscriptions/create', 'SubscriptionController@store')->name('admin.subscription.store');
    Route::get('/subscriptions/edit/{subscription}', 'SubscriptionController@edit')->name('admin.subscription.edit');
    Route::post('/subscriptions/edit/{subscription}', 'SubscriptionController@update')->name('admin.subscription.update');
    Route::get('/subscriptions/delete/{subscription}', 'SubscriptionController@delete')->name('admin.subscription.delete');
    // Route::get('/documentation/category/create', 'DocumentationCatgeoryController@create')->name('admin.documentation.category.create');
    // Route::post('/documentation/category/create', 'DocumentationCatgeoryController@store')->name('admin.documentation.category.store');
    // Route::get('/documentation/category/{category}/edit', 'DocumentationCatgeoryController@edit')->name('admin.documentation.category.edit')->where('category','[0-9]+');;
    // Route::put('/documentation/category/{category}/update', 'DocumentationCatgeoryController@update')->name('admin.documentation.category.update')->where('category','[0-9]+');;

});




